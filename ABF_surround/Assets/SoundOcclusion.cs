﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOcclusion : MonoBehaviour
{
    public AudioSource soundEffect;
    public AudioLowPassFilter lowpassfilter;

    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!checkIfLineOfSight())
        {
            soundEffect.volume = .5f;
            lowpassfilter.enabled = true;
        }
        else
        {
            soundEffect.volume = 1f;
            lowpassfilter.enabled = false;
        }
    }

    bool checkIfLineOfSight()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, (player.transform.position - transform.position), out hit);
        if (hit.collider.tag == "Player")
        {
            Debug.Log("Player in LIne of sight");
            return true;
        }
        else
        {
            Debug.Log("Player occluded");
            return false;
        }
    }
}
