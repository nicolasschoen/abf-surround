﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdMovement : MonoBehaviour
{
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime);

        if(transform.position.x > 650)
        {
            transform.position = new Vector3(350f, Random.Range(25f, 35f), Random.Range(350f, 850f));
        }
        if(transform.position.x < 350)
        {
            transform.position = new Vector3(650f, Random.Range(25f, 35f), Random.Range(350f, 850f));
        }
        if (transform.position.z > 850)
        {
            transform.position = new Vector3(Random.Range(350f, 650f), Random.Range(25f, 35f), 350f);
        }
        if (transform.position.z < 350)
        {
            transform.position = new Vector3(Random.Range(350f, 650f), Random.Range(25f, 35f), 850f);
        }
    }
}
